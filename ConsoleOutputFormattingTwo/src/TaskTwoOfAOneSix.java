
public class TaskTwoOfAOneSix {
	
	public static String calculation(int x) {
		String result = " 1";
		if (x == 0) {}
		else {
			for (int i = 2; i <= x; i++) {
				result = result + " * " + i;
			}
		}
		return result;
	}
	
	public static int factorial(int x) {
		int result = 1;
		if (x == 0) {}
		else {
			for (int i = 1; i <= x; i++) {
				result = result * i;
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int intArray[] = {0,1,2,3,4,5};
		
		for (int i : intArray) {
			System.out.printf("%-5s%s%-19s%s%4d\n", i + "!", "=", calculation(i), "=", factorial(i));	
		}
	}
}
