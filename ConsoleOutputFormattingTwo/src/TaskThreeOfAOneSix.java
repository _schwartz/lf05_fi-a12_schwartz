
public class TaskThreeOfAOneSix {
	
	static double minusTwenty[] = {-20, -28.8889};
	static double minusTen[] = {-10, -23.3333};
	static double zero[] = {0, -17.7778};
	static double twenty[] = {20, -6.6667};
	static double thirty[] = {30, -1.1111};
	
	static double[][] Temperatures = {{-20, -28.8889}, {-10, -23.3333}, {0, -17.7778}, {20, -6.6667}, {30, -1.1111}};
	
	public static void main(String[] args) {
		
		System.out.printf("%-11s|%11s\n", "Fahrenheit", "Celsius");
		
		for (int i = 0; i <= 22; i++) {
			System.out.print("-");
		}
		
		for (int i = 0; i <= Temperatures.length - 1; i++) {
			System.out.printf("\n%-+11f|%11f", Temperatures[i][0], Temperatures[i][1]);
		}
		
		System.out.print("\n");
	}
}
