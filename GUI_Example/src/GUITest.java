import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GUITest extends JDialog {
    private JPanel contentPane;
    private JButton btnRedBackground;
    private JButton btnGreenBackground;
    private JButton btnBlueBackground;
    private JButton btnYellowBackground;
    private JButton btnStandardColorBackground;
    private JButton btnChooseColorBackground;
    private JPanel pnlBase;
    private JPanel pnlColorButtons;
    private JLabel txtTask1;
    private JLabel txtTask2;
    private JPanel pnlFormatText;
    private JButton btnArial;
    private JButton btnStandardFont;
    private JButton btnCourierNew;
    private JTextField textField1;
    private JButton btnWriteToTextField;
    private JButton btnDeleteTextFieldData;
    private JPanel pnlTextField;
    private JLabel txtTask4;
    private JButton btnRedForeground;
    private JButton btnBlueForeground;
    private JButton btnBlackForeground;
    private JPanel pnlTextColors;
    private JLabel txtTask5;
    private JPanel pnlInAndDecreaseFontSize;
    private JButton btnIncreaseFontSize;
    private JButton btnDecreaseFontSize;
    private JLabel txtTask6;
    private JButton btnAlignLeft;
    private JButton btnAlignCenter;
    private JButton btnAlignRight;
    private JPanel pnlAlignText;
    private JLabel txtTask7;
    private JButton btnExit;

    private final Color STANDARD_COLOR = contentPane.getBackground();
    private final Font STANDARD_FONT = txtTask1.getFont();
    private final String STANDARD_FONT_TYPE = STANDARD_FONT.getFontName();
    private final int STANDARD_FONT_SIZE = txtTask1.getFont().getSize();
    private int currentFontSize = STANDARD_FONT_SIZE;
    public GUITest() {
        setContentPane(contentPane);
        setModal(true);

        // getRootPane().setDefaultButton(buttonOK);

        txtTask1.setText("Aufgabe 1: Hintergrundfarbe ändern");

        btnRedBackground.setText("Rot");
        btnRedBackground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnRedBackground();
            }
        });

        btnGreenBackground.setText("Grün");
        btnGreenBackground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnGreen();
            }
        });

        btnBlueBackground.setText("Blau");
        btnBlueBackground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnBlue();
            }
        });

        btnYellowBackground.setText("Gelb");
        btnYellowBackground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnYellow();
            }
        });

        btnStandardColorBackground.setText("Standardfarbe");
        btnStandardColorBackground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnStandardColor();
            }
        });

        btnChooseColorBackground.setText("Farbe wählen");
        btnChooseColorBackground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnChooseColor();
            }
        });

        textField1.setText("Aufgabe 2: Text formatieren");

        btnArial.setText("Arial");
        btnArial.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnArial();
            }
        });

        btnStandardFont.setText("Standard Font");
        btnStandardFont.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnStandardFont();
            }
        });


        btnCourierNew.setText("Courier New");
        btnCourierNew.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnCourierNew();
            }
        });

        textField1.setForeground(new Color(100, 100, 100));
        textField1.setText("Hier bitte Text eingeben");

        btnWriteToTextField.setText("Ins Label schreiben");
        btnWriteToTextField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnWriteToTextField();
            }
        });

        btnDeleteTextFieldData.setText("Text im Label löschen");
        btnDeleteTextFieldData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnDeleteTextFieldData();
            }
        });


        txtTask4.setText("Aufgabe 3: Schriftfarbe ändern");

        btnRedForeground.setText("Rote Schriftfarbe");
        btnRedForeground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnRedForeground();
            }
        });

        btnBlueForeground.setText("Blaue Schriftfarbe");
        btnBlueForeground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnBlueForeground();
            }
        });

        btnBlackForeground.setText("Schwarze Schriftfarbe");
        btnBlackForeground.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnBlackForeground();
            }
        });

        txtTask5.setText("Aufgabe 4: Schriftgröße verändern (%d)".formatted(STANDARD_FONT_SIZE));

        btnIncreaseFontSize.setText("+");
        btnIncreaseFontSize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnIncreaseFontSize();
            }
        });

        btnDecreaseFontSize.setText("-");
        btnDecreaseFontSize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnDecreaseFontSize();
            }
        });

        txtTask6.setText("Aufgabe 5: Textausrichtung");

        btnAlignLeft.setText("linksbünding");
        btnAlignLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnAlignLeft();
            }
        });

        btnAlignCenter.setText("zentriert");
        btnAlignCenter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnAlignCenter();
            }
        });

        btnAlignRight.setText("rechtsbündig");
        btnAlignRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnAlignRight();
            }
        });

        txtTask7.setText("Aufgabe 6: Programm beenden");

        btnExit.setText("Exit");
        btnExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onBtnExit();
            }
        });


        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                exit();
            }
        });

//        // call onCancel() on ESCAPE
//        contentPane.registerKeyboardAction(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                onCancel();
//            }
//        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void exit() { dispose(); }

    private void onBtnRedBackground() { setBackgroundColors(Color.RED); }
    private void onBtnGreen() { setBackgroundColors(Color.GREEN); }
    private void onBtnBlue() { setBackgroundColors(Color.BLUE); }
    private void onBtnYellow() { setBackgroundColors(Color.YELLOW); }
    private void onBtnStandardColor() { setBackgroundColors(STANDARD_COLOR); }
    private void onBtnChooseColor() {
        Color newColor = JColorChooser.showDialog(contentPane, "Wähle eine Farbe", STANDARD_COLOR);
        if (newColor != null) { setBackgroundColors(newColor); }
    }

    private void onBtnArial() { setFonts("Arial"); }
    private void onBtnStandardFont() { setFonts(STANDARD_FONT_TYPE); }
    private void onBtnCourierNew() { setFonts("Courier New"); }
    private void onBtnWriteToTextField() {
        textField1.requestFocusInWindow();
    }
    private void onBtnDeleteTextFieldData() {
        textField1.setText("");
    }

    private void onBtnRedForeground() { setForegroundColors(Color.RED); }
    private void onBtnBlueForeground() { setForegroundColors(Color.BLUE); }
    private void onBtnBlackForeground() { setForegroundColors(Color.BLACK); }

    private void onBtnIncreaseFontSize() { if (currentFontSize < 24) { currentFontSize += 1; setFontSize(currentFontSize); } }
    private void onBtnDecreaseFontSize() { if (currentFontSize > 8) { currentFontSize -= 1; setFontSize(currentFontSize); } }

    private void onBtnAlignLeft() { alignText(SwingConstants.LEFT); }
    private void onBtnAlignCenter() { alignText(SwingConstants.CENTER); }
    private void onBtnAlignRight() { alignText(SwingConstants.RIGHT); }

    private void onBtnExit() { exit(); }
    private void setBackgroundColors(Color color) {
        contentPane.setBackground(color);

        pnlBase.setBackground(color);
        pnlColorButtons.setBackground(color);
        pnlFormatText.setBackground(color);
        pnlTextField.setBackground(color);
        pnlTextColors.setBackground(color);
        pnlInAndDecreaseFontSize.setBackground(color);
        pnlAlignText.setBackground(color);
    }

    private void setFonts(String font) {
        txtTask1.setFont(new Font(font, txtTask1.getFont().getStyle(), txtTask1.getFont().getSize()));
        txtTask2.setFont(new Font(font, txtTask2.getFont().getStyle(), txtTask2.getFont().getSize()));
        txtTask4.setFont(new Font(font, txtTask4.getFont().getStyle(), txtTask4.getFont().getSize()));
        txtTask5.setFont(new Font(font, txtTask4.getFont().getStyle(), txtTask4.getFont().getSize()));
        txtTask6.setFont(new Font(font, txtTask6.getFont().getStyle(), txtTask6.getFont().getSize()));
        txtTask7.setFont(new Font(font, txtTask7.getFont().getStyle(), txtTask7.getFont().getSize()));

        textField1.setFont(new Font(font, textField1.getFont().getStyle(), textField1.getFont().getSize()));

        btnRedBackground.setFont(new Font(font, btnRedBackground.getFont().getStyle(), btnRedBackground.getFont().getSize()));
        btnGreenBackground.setFont(new Font(font, btnGreenBackground.getFont().getStyle(), btnGreenBackground.getFont().getSize()));
        btnBlueBackground.setFont(new Font(font, btnBlueBackground.getFont().getStyle(), btnBlueBackground.getFont().getSize()));
        btnYellowBackground.setFont(new Font(font, btnYellowBackground.getFont().getStyle(), btnYellowBackground.getFont().getSize()));
        btnStandardColorBackground.setFont(new Font(font, btnStandardColorBackground.getFont().getStyle(), btnStandardColorBackground.getFont().getSize()));
        btnChooseColorBackground.setFont(new Font(font, btnChooseColorBackground.getFont().getStyle(), btnChooseColorBackground.getFont().getSize()));

        btnWriteToTextField.setFont(new Font(font, btnWriteToTextField.getFont().getStyle(), btnWriteToTextField.getFont().getSize()));
        btnDeleteTextFieldData.setFont(new Font(font, btnDeleteTextFieldData.getFont().getStyle(), btnDeleteTextFieldData.getFont().getSize()));

        btnArial.setFont(new Font(font, btnArial.getFont().getStyle(), btnArial.getFont().getSize()));
        btnStandardFont.setFont(new Font(font, btnStandardFont.getFont().getStyle(), btnStandardFont.getFont().getSize()));
        btnCourierNew.setFont(new Font(font, btnCourierNew.getFont().getStyle(), btnCourierNew.getFont().getSize()));

        btnWriteToTextField.setFont(new Font(font, btnWriteToTextField.getFont().getStyle(), btnWriteToTextField.getFont().getSize()));
        btnDeleteTextFieldData.setFont(new Font(font, btnDeleteTextFieldData.getFont().getStyle(), btnDeleteTextFieldData.getFont().getSize()));

        btnRedForeground.setFont(new Font(font, btnRedForeground.getFont().getStyle(), btnRedForeground.getFont().getSize()));
        btnBlueForeground.setFont(new Font(font, btnBlueForeground.getFont().getStyle(), btnBlueForeground.getFont().getSize()));
        btnBlackForeground.setFont(new Font(font, btnBlackForeground.getFont().getStyle(), btnBlackForeground.getFont().getSize()));

        btnIncreaseFontSize.setFont(new Font(font, btnIncreaseFontSize.getFont().getStyle(), btnIncreaseFontSize.getFont().getSize()));
        btnDecreaseFontSize.setFont(new Font(font, btnDecreaseFontSize.getFont().getStyle(), btnDecreaseFontSize.getFont().getSize()));

        btnAlignLeft.setFont(new Font(font, btnAlignLeft.getFont().getStyle(), btnAlignLeft.getFont().getSize()));
        btnAlignCenter.setFont(new Font(font, btnAlignCenter.getFont().getStyle(), btnAlignCenter.getFont().getSize()));
        btnAlignRight.setFont(new Font(font, btnAlignRight.getFont().getStyle(), btnAlignRight.getFont().getSize()));

        btnExit.setFont(new Font(font, btnExit.getFont().getStyle(), btnExit.getFont().getSize()));
    }

    private void setForegroundColors(Color color) {
        txtTask1.setForeground(color);
        txtTask2.setForeground(color);
        txtTask4.setForeground(color);
        txtTask5.setForeground(color);
        txtTask6.setForeground(color);
        txtTask7.setForeground(color);

        textField1.setForeground(color);

        btnRedBackground.setForeground(color);
        btnGreenBackground.setForeground(color);
        btnBlueBackground.setForeground(color);
        btnYellowBackground.setForeground(color);
        btnStandardColorBackground.setForeground(color);
        btnChooseColorBackground.setForeground(color);

        btnWriteToTextField.setForeground(color);
        btnDeleteTextFieldData.setForeground(color);

        btnArial.setForeground(color);
        btnStandardFont.setForeground(color);
        btnCourierNew.setForeground(color);

        btnWriteToTextField.setForeground(color);
        btnDeleteTextFieldData.setForeground(color);

        btnRedForeground.setForeground(color);
        btnBlueForeground.setForeground(color);
        btnBlackForeground.setForeground(color);

        btnIncreaseFontSize.setForeground(color);
        btnDecreaseFontSize.setForeground(color);

        btnAlignLeft.setForeground(color);
        btnAlignCenter.setForeground(color);
        btnAlignRight.setForeground(color);

        btnExit.setForeground(color);
    }

    private void setFontSize(int size) {
        txtTask5.setText("Aufgabe 4: Schriftgröße verändern (%d)".formatted(size));
        txtTask1.setFont(new Font(txtTask1.getFont().getFontName(), txtTask1.getFont().getStyle(), size));
        txtTask2.setFont(new Font(txtTask2.getFont().getFontName(), txtTask2.getFont().getStyle(), size));
        txtTask4.setFont(new Font(txtTask4.getFont().getFontName(), txtTask4.getFont().getStyle(), size));
        txtTask5.setFont(new Font(txtTask5.getFont().getFontName(), txtTask5.getFont().getStyle(), size));
        txtTask6.setFont(new Font(txtTask6.getFont().getFontName(), txtTask6.getFont().getStyle(), size));
        txtTask7.setFont(new Font(txtTask7.getFont().getFontName(), txtTask7.getFont().getStyle(), size));

        textField1.setFont(new Font(textField1.getFont().getFontName(), textField1.getFont().getStyle(), size));

        btnRedBackground.setFont(new Font(btnRedBackground.getFont().getFontName(), btnRedBackground.getFont().getStyle(), size));
        btnGreenBackground.setFont(new Font(btnGreenBackground.getFont().getFontName(), btnGreenBackground.getFont().getStyle(), size));
        btnBlueBackground.setFont(new Font(btnBlueBackground.getFont().getFontName(), btnBlueBackground.getFont().getStyle(), size));
        btnYellowBackground.setFont(new Font(btnYellowBackground.getFont().getFontName(), btnYellowBackground.getFont().getStyle(), size));
        btnStandardColorBackground.setFont(new Font(btnStandardColorBackground.getFont().getFontName(), btnStandardColorBackground.getFont().getStyle(), size));
        btnChooseColorBackground.setFont(new Font(btnChooseColorBackground.getFont().getFontName(), btnChooseColorBackground.getFont().getStyle(), size));

        btnWriteToTextField.setFont(new Font(btnWriteToTextField.getFont().getFontName(), btnWriteToTextField.getFont().getStyle(), size));
        btnDeleteTextFieldData.setFont(new Font(btnDeleteTextFieldData.getFont().getFontName(), btnDeleteTextFieldData.getFont().getStyle(), size));

        btnArial.setFont(new Font(btnArial.getFont().getFontName(), btnArial.getFont().getStyle(), size));
        btnStandardFont.setFont(new Font(btnStandardFont.getFont().getFontName(), btnStandardFont.getFont().getStyle(), size));
        btnCourierNew.setFont(new Font(btnCourierNew.getFont().getFontName(), btnCourierNew.getFont().getStyle(), size));

        btnWriteToTextField.setFont(new Font(btnWriteToTextField.getFont().getFontName(), btnWriteToTextField.getFont().getStyle(), size));
        btnDeleteTextFieldData.setFont(new Font(btnDeleteTextFieldData.getFont().getFontName(), btnDeleteTextFieldData.getFont().getStyle(), size));

        btnRedForeground.setFont(new Font(btnRedForeground.getFont().getFontName(), btnRedForeground.getFont().getStyle(), size));
        btnBlueForeground.setFont(new Font(btnBlueForeground.getFont().getFontName(), btnBlueForeground.getFont().getStyle(), size));
        btnBlackForeground.setFont(new Font(btnBlackForeground.getFont().getFontName(), btnBlackForeground.getFont().getStyle(), size));

        btnIncreaseFontSize.setFont(new Font(btnIncreaseFontSize.getFont().getFontName(), btnIncreaseFontSize.getFont().getStyle(), size));
        btnDecreaseFontSize.setFont(new Font(btnDecreaseFontSize.getFont().getFontName(), btnDecreaseFontSize.getFont().getStyle(), size));

        btnAlignLeft.setFont(new Font(btnAlignLeft.getFont().getFontName(), btnAlignLeft.getFont().getStyle(), size));
        btnAlignCenter.setFont(new Font(btnAlignCenter.getFont().getFontName(), btnAlignCenter.getFont().getStyle(), size));
        btnAlignRight.setFont(new Font(btnAlignRight.getFont().getFontName(), btnAlignRight.getFont().getStyle(), size));

        btnExit.setFont(new Font(btnExit.getFont().getFontName(), btnExit.getFont().getStyle(), size));
    }

    private void alignText(int swing) {
        txtTask1.setHorizontalAlignment(swing);
        txtTask2.setHorizontalAlignment(swing);
        txtTask4.setHorizontalAlignment(swing);
        txtTask5.setHorizontalAlignment(swing);
        txtTask6.setHorizontalAlignment(swing);
        txtTask7.setHorizontalAlignment(swing);

        textField1.setHorizontalAlignment(swing);

        btnRedBackground.setHorizontalAlignment(swing);
        btnGreenBackground.setHorizontalAlignment(swing);
        btnBlueBackground.setHorizontalAlignment(swing);
        btnYellowBackground.setHorizontalAlignment(swing);
        btnStandardColorBackground.setHorizontalAlignment(swing);
        btnChooseColorBackground.setHorizontalAlignment(swing);

        btnWriteToTextField.setHorizontalAlignment(swing);
        btnDeleteTextFieldData.setHorizontalAlignment(swing);

        btnArial.setHorizontalAlignment(swing);
        btnStandardFont.setHorizontalAlignment(swing);
        btnCourierNew.setHorizontalAlignment(swing);

        btnWriteToTextField.setHorizontalAlignment(swing);
        btnDeleteTextFieldData.setHorizontalAlignment(swing);

        btnRedForeground.setHorizontalAlignment(swing);
        btnBlueForeground.setHorizontalAlignment(swing);
        btnBlackForeground.setHorizontalAlignment(swing);

        btnIncreaseFontSize.setHorizontalAlignment(swing);
        btnDecreaseFontSize.setHorizontalAlignment(swing);

        btnAlignLeft.setHorizontalAlignment(swing);
        btnAlignCenter.setHorizontalAlignment(swing);
        btnAlignRight.setHorizontalAlignment(swing);

        btnExit.setHorizontalAlignment(swing);
    }

    public static void main(String[] args) {
        GUITest dialog = new GUITest();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
