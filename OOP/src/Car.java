import java.util.ArrayList;

public class Car {
    private String brandName;
    private String color;
    ArrayList<Cargo> cargos = new ArrayList<Cargo>();

    // Why
    public Car(String brandName, String color) {
        this.brandName = brandName;
        this.color = color;

    }

    // Settings
    public void addCargo(String kind, double amount) {
        this.cargos.add(new Cargo(kind, amount));
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // Prints
    public String getBrandName() {
        return this.brandName;
    }

    public String getColor() {
        return this.color;
    }

    public ArrayList<Cargo> getCargos() {
        return this.cargos;
    }
}
