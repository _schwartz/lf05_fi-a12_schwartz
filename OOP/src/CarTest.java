import java.util.Scanner;

public class CarTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        Car a1 = new Car("DeLorean DMC 12", "silver");

        a1.addCargo("Coca Cola", 1);

        // System.out.println(a1.brandName);
        System.out.println(a1.getBrandName());
        System.out.println(a1.getColor());

        System.out.println(a1.getCargos());
    }
}
