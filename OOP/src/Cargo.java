public class Cargo {
    private String kind;
    private double amount;
    private String description;

    public Cargo(String kind, double amount) {
        this.kind = kind;
        this.amount = amount;
    }

    // Settings
    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setDescription(String  description) {
        this.description = description;
    }

    // Prints
    public String getKind() {
        return this.kind;
    }

    public double getAmount() {
        return this.amount;
    }

    public String  getDescription() {
        return this.description;
    }
}
