// average.java

import java.util.Scanner;


public class average {

	// calculate average of x and y
	public static double calculateAverage( double x, double y ) {
		return (x + y) / 2.0;
	}
	
	public static double inp(String s) {
		Scanner keyboard = new Scanner(System.in);
		System.out.printf("\nInput a double '%s': ", s);
		double val = keyboard.nextDouble();
		return val;
	}
	
	public static void main(String[] args) {

		// initilization
		double x;
		double y;
		double m;
		
		// get x and y
		x = inp("x");
		y = inp("y");

		// calculate average of x and y
		m = calculateAverage(x, y);

		// printing result
		System.out.printf("\nThe average of %.2f and %.2f equals %.2f\n", x, y, m);
	}
}
