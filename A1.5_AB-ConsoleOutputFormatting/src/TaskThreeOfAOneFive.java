
public class TaskThreeOfAOneFive {
	
	static String num1 = String.format("%.2f", 22.4234234);
	static String num2 = String.format("%.2f", 111.2222);
	static String num3 = String.format("%.2f", 4.0);
	static String num4 = String.format("%.2f", 1000000.551);
	static String num5 = String.format("%.2f", 97.34);
	
	static String first = num1.replace('.', ',');
	static String second = num2.replace('.', ',');
	static String third = num3.replace('.', ',');
	static String forth = num4.replace('.', ',');
	static String fifth = num5.replace('.', ',');
	
	public static void main(String[] args) {
		System.out.printf("%s\n%s\n%sf\n%s\n%s\n", first, second, third, forth, fifth);
	}
}
