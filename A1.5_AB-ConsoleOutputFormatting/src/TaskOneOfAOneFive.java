
public class TaskOneOfAOneFive {
	
	static String stringone = "first string";
	static String stringtwo = "second string";

	public static void main(String[] args) {
		System.out.printf("\n\'%s\'\n\'%s\'\n", stringone, stringtwo);

	}
}

// print() simply prints the given value and println() will also print a break (\n) at the end of the line
