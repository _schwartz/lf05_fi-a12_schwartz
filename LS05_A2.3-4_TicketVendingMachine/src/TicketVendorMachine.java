import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class TicketVendorMachine {
    public static Scanner keyboard = new Scanner(System.in);

    public int number;
    public double prize;
    public String name;
    public String description;

    public TicketVendorMachine(int number, double prize, String name, String description) {
        this.number = number;
        this.prize = prize;
        this.name = name;
        this.description = description;
    }

    // Not Mine
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
	/*
    public static double inputDouble() {
        Double d = null;
        do {
            try {
                d = keyboard.nextDouble();
                keyboard.nextLine();
            } catch (Exception e) {
                System.out.println("\nWrong input. Please try again:");
            }
        } while (d == null);
        return d;
    }
	*/

	public static double inputDouble() {
		Double d = null;
		do {
			String n = keyboard.nextLine();
			try {
				d = Double.parseDouble(n);

			} catch (NumberFormatException e) {
				System.out.println(e);
				System.out.println("\nWrong input. Please try again:");
			}
		} while (d == null);
		return d;
	}

	/*
	public static int inputInt() {
		Integer i = null;
		do {
			try {
				i = keyboard.nextInt();
				keyboard.nextLine();
			} catch (Exception e) {
				System.out.println("\nWrong input. Please try again:");
			}
		} while (i == null);
		return i;
	}
	*/

    public static int inputInt() {
        Integer i = null;
        do {
            String n = keyboard.nextLine();
            try {
                i = Integer.parseInt(n);
            } catch (NumberFormatException e) {
                System.out.println("\nWrong input. Please try again:");
            }
        } while (i == null);
        return i;
    }

    public static String inputString() {
        try {
            return keyboard.next();
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean yesOrNo() {
        String bool = inputString();

        if (bool.equals("y") || bool.equals("Y") || bool.equals("yes") || bool.equals("Yes") || bool.equals("YES")) {
            return true;
        } else if (bool.equals("n") || bool.equals("N") || bool.equals("no") || bool.equals("No") || bool.equals("NO")) {
            return false;
        } else {
            System.out.println("Wrong input. Please try again.");
            return yesOrNo();
        }
    }

    public static double customForEachTicket(ArrayList<Integer> ticket, TicketVendorMachine[] possible_tickets) {
        return possible_tickets[ticket.get(0) - 1].prize * ticket.get(1);
    }

    private static int tType() {
        int ticket_type;

        while (true) {
            System.out.print("\nWhat ticket do you want to purchase: ");
            ticket_type = inputInt();

            if (ticket_type == 1 || ticket_type == 2 || ticket_type == 3) {
                return ticket_type;
            } else {
                System.out.printf("\nUnsupported input. Please try again.\n\n");
            }
        }
    }

    private static int tAmount(int ticket_type) {
        // IF THE USER INPUT IS BAD, IT WILL NOT SIMPLY SET THE AMOUNT TO 1 BUT RATHER ASK THE USER TO TRY AGAIN.

        System.out.printf("\n\nHow many tickets of id %s would you like to get (max. 10): ", ticket_type);
        int ticket_amount = inputInt();

        if (ticket_amount >= 1 && ticket_amount <= 10) {
            return ticket_amount;
        } else {
            System.out.printf("\nUsupported input. Please try again:\n");
            return tAmount(ticket_type);
        }
    }

    private static double insert_coin() {
        while (true) {
            double inserted_coin = inputDouble();

            if ((inserted_coin != 2 && inserted_coin != 1 && inserted_coin != .5 && inserted_coin != .2 && inserted_coin != .1 && inserted_coin != .05) && inserted_coin == (double) inserted_coin) {
                System.out.print("\nPlease only use 2 Euro, 1 Euro, 50 cent, 20 cent, 10 cent or 5 cent:");
            } else {
                return inserted_coin;
            }
        }
    }

    private static double payment(double payment_amount) {
        double total_amount_paid = 0.0;

        total_amount_paid = round(total_amount_paid, 2);

        System.out.printf("Ticket price (in Euro): %.2f ", payment_amount);

        while (total_amount_paid < payment_amount) {
            System.out.printf("\nYet to be payed: %.2f Euro\n", (payment_amount - total_amount_paid));

            System.out.println("\nEntry (min. 5 cent, max. 2 Euro):");

            total_amount_paid += insert_coin();
        }
        return total_amount_paid;
    }

    private static double prize(List<ArrayList<Integer>> tickets, TicketVendorMachine[] possible_tickets) {
        int tickets_length = tickets.size();
        double prize = 0.0;

        for (int i = 0; i < tickets_length; i++) {
            prize = prize + customForEachTicket(tickets.get(i), possible_tickets);
        }
        return prize;
    }

    private static void printTicket() {
        System.out.println("\nTicket is getting printed ...");

        for (int i = 0; i < 29; i++) {
            System.out.print("=");

            try {
                Thread.sleep((int) Math.floor(Math.random() * (200 - 10 + 1) + 10));

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    private static void returnMoneyAsCoin(double return_amount) {
        System.out.printf("\n   * * *\n *       *\n*  0%.2f  *\n*  €EURO  *\n *       *\n   * * *\n", return_amount);
    }


    private static void returnMoney(double unrounded_return_amount) {
        double return_amount = round(unrounded_return_amount * 100, 2);
        if (return_amount > 0) {
            System.out.printf("Return amount: %.2f Euro\n\n", return_amount);
            System.out.println("Will be return as follows:");

            while (return_amount >= 200) {
                // System.out.println("2 Euro");
                returnMoneyAsCoin(2);
                return_amount -= 200;
            }
            while (return_amount >= 100) {
                // System.out.println("1 Euro");
                returnMoneyAsCoin(1);
                return_amount -= 100;
            }
            while (return_amount >= 50) {
                // System.out.println("50 Cent");
                returnMoneyAsCoin(.5);
                return_amount -= 50;
            }
            while (return_amount >= 20) {
                // System.out.println("20 Cent");
                returnMoneyAsCoin(.2);
                return_amount -= 20;
            }
            while (return_amount >= 10) {
                // System.out.println("10 Cent");
                returnMoneyAsCoin(.1);
                return_amount -= 10;
            }
            while (return_amount >= 5) {
                // System.out.println("5 Cent");
                returnMoneyAsCoin(.05);
                return_amount -= 5;
            }
        }
    }

    public static void main(String[] args) {
        Integer amount_of_possible_tickets = 3;
        TicketVendorMachine[] possible_tickets;
        possible_tickets = new TicketVendorMachine[amount_of_possible_tickets];

        possible_tickets[0] = new TicketVendorMachine(1, 2, "Short-haul tariff", " # 3 stations in Berlin using the S- or U-Bahn\n"
                + " # (switch is allowed). OR up to 6 stations going\n"
                + " # by tram or bus, up to 4 stations in the City of\n"
                + " # Potsdam, while transport means are not regulated\n");

        possible_tickets[1] = new TicketVendorMachine(2, 3, "Single fare ticket (AB)", " # Valid: In the tariff area of Berlin: 120 minutes)\n");

        possible_tickets[2] = new TicketVendorMachine(3, 3, "Extension ticket (ABC)", " # Valid: In the tariff area of Berlin: 120 minutes)\n");

        while (true) {
            System.out.println("Welcome!");

            boolean continuing = true;
            double payment_amount = 0.0;
            double total_amount_paid = 0.0;
            int ticket_type;

            List<ArrayList<Integer>> tickets = new ArrayList<ArrayList<Integer>>();

            while (continuing) {
                System.out.print("\nPossible tickets:\n\n");

                for (int i = 0; i < amount_of_possible_tickets; i++) {
                    System.out.printf("%d %s (%.2f €)\n%s\n", i, possible_tickets[i].name, possible_tickets[i].prize, possible_tickets[i].description);
                }

                ArrayList<Integer> ticket = new ArrayList<Integer>();

                ticket_type = tType();
                ticket.add(ticket_type);

                ticket.add(tAmount(ticket_type));

                tickets.add(ticket);

                payment_amount = prize(tickets, possible_tickets);

                System.out.printf("\nCurrent prize: %.2f, \nDo you want to buy more tickets (y/n)\n", payment_amount);
                if (!yesOrNo()) {
                    continuing = false;
                }
            }

            total_amount_paid = payment(payment_amount);

            printTicket();

            returnMoney(total_amount_paid - payment_amount);

            System.out.println("\nDo not forget to validate your\n" +
                    "ticket before starting your journey!\n" +
                    "Have a pleasant trip.");

            try {
                Thread.sleep(3000);

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}