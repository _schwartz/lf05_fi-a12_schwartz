public class OddNumbers {
    public static void main(String[] args) {
        int[] arr = new int[10];
        int index = 0;

        for (int i = 1; i < 19; i++) {
            if (i % 2 == 0) {
                arr[index++] = i;
            }
        }

        for (int i = 0; i < arr.length - 1; i++) {
            System.out.println(arr[i]);
        }
    }
}
