// import java.util.Arrays;

public class Lottery {
    public static boolean in(int[] arr, int val){
        boolean result = false;

        for (int num : arr) {
            if (val == num) {
                result = true;
                break;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        final int[] lottery_numbers = new int[] {3, 7, 12, 18, 37, 42};
        int[] numbers = new int[] {12, 13};

        // System.out.println(Arrays.toString(lottery_numbers));

        System.out.print("[ ");
        for (int num : lottery_numbers) {
            System.out.print(num + " ");
        }
        System.out.println("]\n");

        for (int number : numbers) {
            System.out.printf("The number %s is" +  (in(lottery_numbers, number) ? "" : "not ") + "included in the draw.\n", number);

        }
    }
}
