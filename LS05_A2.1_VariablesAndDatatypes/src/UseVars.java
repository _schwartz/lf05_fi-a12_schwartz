// package eingabeAusgabe;

/** Varsiables.java
 * Add fitting code to every comment.
 * @author J. Schwartz
 * @version 1.0
*/


import java.util.Scanner;


public class UseVars {
	
	static double minusPower(double x, double y) {
		double result = x;
		for (int i = 0; i <= y; i++) {
			result = result / x;
		}
	    return result;
	}
	
  public static void main(String [] args){
    /* 01. */
    int counter;

    /* 02. */
    counter = 25;

    /* 03. */
      Scanner menuEntry = new Scanner(System.in);
      System.out.print("Enter Menu entry and press 'Enter': ");

    /* 04. */
      String C = menuEntry.nextLine();
      System.out.println("\nMenu entry is: " + C);

    /* 05. */
      long longNumber;

    /* 06. */
      longNumber = 299792458L;
      System.out.println("The Speed of light is " + longNumber + " m/s.");

    /* 07. */
      int members = 7;

    /* 08. */
      System.out.println("The Club has " + members + " members.");

    /* 09. */
      double elementaryCharge = minusPower(10, 19) * 1.6021766; //1.6021766e-19 coulombs
      System.out.println("The elementary charge is " + elementaryCharge + " coulombs.");

    /* 10. */
      boolean payment;

    /* 11. */
      payment = true;
      System.out.println("The payment was succesfull: " + payment);
  }
}
