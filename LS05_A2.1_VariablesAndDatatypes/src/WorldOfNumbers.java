/**
  *   Task:  Get the information form the Internet!
  * 
  *   Don't change the name of the variables!
  *
  *   Don't forget to choose the right data type!
  *
  *
  * @version 1.0 from 2021-09-17
  * @author << J. Schwartz >>
  */

public class WorldOfNumbers {

  public static void main(String[] args) {
    
    /* Assignment of variables! */
                    
	// Pluto doesn't count as a Planet
    int anzahlPlanen =  8;
    
    // Maximum predicted number of stars
    long anzahlSterne = 400000000000L;
    
    // On September 17th 2021
    int bewohnerBerlin = 3645000;
    
    // On September 17th 2021
    int alterTage = 6120;
    
    // Antarctic Blue Whale
    int gewichtKilogramm = 181437; 
    
    // Biggest country on earth in square kilometers: Russia
    int flaecheGroessteLand = 17000000;
    
    // Smallest country on earth in square kilometers: Vatican City
    float flaecheKleinsteLand = 0.49f;
    
    
    /* Printing the variables  */
    
    System.out.println("Quantity of planets in our solar system: " + anzahlPlanen);
    System.out.println("\nQuantity of stars in our milky way: " + anzahlSterne);
	System.out.println("Inhabitants of Berlin: " + bewohnerBerlin);
	System.out.println("My age in days: " + alterTage);
	System.out.println("The heaviest animal on earth in kilograms: " + gewichtKilogramm);
	System.out.println("The biggest country on earth in square kilometers: " + flaecheGroessteLand);
	System.out.println("The smallest country on earth in square kilometers: " + flaecheKleinsteLand);
    System.out.println("\n *******  End  ******* ");
  }
}
