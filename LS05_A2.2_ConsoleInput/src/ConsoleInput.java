import java.util.Scanner;

public class ConsoleInput {
	public static void main(String[] args)
	 {
	 // Creating "Scanner" object
	 Scanner myScanner = new Scanner(System.in);

	 System.out.print("\nPlease enter an integer: ");

	 // Saving first input as num1
	 int num1 = myScanner.nextInt();

	 System.out.print("\nPlease enter anther integer: ");

	 // Saving second input as num2
	 int num2 = myScanner.nextInt();

	 // Adding both inputs and saving them as result
	 int result = num1 + num2;

	 System.out.print("\n\n\nThe result of the addition is: ");
	 System.out.print(num1 + " + " + num2 + " = " + result);
	 
	 // Closing "Scanner"
	 myScanner.close();
	 }
}
