import java.util.ArrayList;

/*
 * A4.1.1
 */

public class UseCodeA4_1_1 {
    public static void main(String[] args) {

        ArrayList<String> broadcastCommunicator = new ArrayList<String>();


        /*
         * klingons
         */

        Spaceship klingons = new Spaceship("IKS Hegh'ta");

        // Attribute photonTorpedoes
        klingons.setPhotonTorpedoes(1);

        // Attribute energyLevelInPercent is 100 by default

        // Attribute shellProtectionInPercent is 100 by default

        // Attribute shieldProtectionInPercent is 100 by default

        // Attribute lifeSupportFunctionalityInPercent is 100 by default

        // Attribute name is set using initiation

        // Attribute androidsAmount
        klingons.setAndroidsAmount(2);

        // Cargo 1 "Ferengi snail Juice"
        klingons.createCargo("Ferengi snail Juice", 200);

        // Cargo 2 "Bat'leth Klingons Sword"
        klingons.createCargo("Bat'leth Klingons Sword", 200);



        /*
         * romulans
         */

        Spaceship romulans = new Spaceship("IWR Khazara");

        // Attribute photonTorpedoes
        romulans.setPhotonTorpedoes(2);

        // Attribute energyLevelInPercent is 100 by default

        // Attribute shellProtectionInPercent is 100 by default

        // Attribute shieldProtectionInPercent is 100 by default

        // Attribute lifeSupportFunctionalityInPercent is 100 by default

        // Attribute name is set using initiation

        // Attribute androidsAmount
        romulans.setAndroidsAmount(2);


        // Cargo 1 "Borg Trash"
        romulans.createCargo("Borg Trash", 5);

        // Cargo 2 "Red Matter"
        romulans.createCargo("Red Matter", 2);

        // Cargo 3 "Plasma Weapon"
        romulans.createCargo("Plasma Weapon", 50);



        /*
         * vulcans
         */

        Spaceship vulcans = new Spaceship("Ni'var");

        // Attribute photonTorpedoes is 0 by default

        // Attribute energyLevelInPercent is 100 by default
        vulcans.setEnergyLevelInPercent(80);

        // Attribute shellProtectionInPercent is 100 by default
        vulcans.setShellProtectionInPercent(50);

        // Attribute shieldProtectionInPercent is 100 by default
        vulcans.setShieldProtectionInPercent(80);

        // Attribute lifeSupportFunctionalityInPercent is 100 by default

        // Attribute name is set using initiation

        // Attribute androidsAmount
        vulcans.setAndroidsAmount(5);


        // Cargo 1 Probe
        vulcans.createCargo("Probe", 50);

        // Cargo 2 PhotonTorpedoes
        vulcans.createCargo("PhotonTorpedoes", 3);
    }
}
