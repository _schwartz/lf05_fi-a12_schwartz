public class Cargo {
    private String kind;
    private double amount;

    public Cargo(String kind, double amount) {
        this.kind = kind;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Cargo{" +
                "kind='" + kind + '\'' +
                ", amount=" + amount +
                '}';
    }

    /*
     * Setting
     */

    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    /*
     * Getting
     */

    public String getKind() {
        return this.kind;
    }

    public double getAmount() {
        return this.amount;
    }
}
