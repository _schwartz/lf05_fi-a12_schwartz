import java.util.Objects;

public class CargoTest {
    public static void main(String[] args) {
        Cargo testInstance = new Cargo("CargoTestInstance", 1.0);

        /*
         * Setting And Getting
         */

        String kind = "Coca Cola Cargo";
        testInstance.setKind(kind);
        if (!Objects.equals(testInstance.getKind(), kind)) {
            System.out.println("<!> ERROR: testInstance -> kind" + "\"");
        }

        double amount = 2.0;
        testInstance.setAmount(amount);
        if (testInstance.getAmount() != amount) {
            System.out.println("<!> ERROR: testInstance -> amount" + "\"");
        }
    }
}
