import java.util.ArrayList;

public class ShootingTask {
    public static void main(String[] args) {

        /*
         * LS02-5-02.1 – Shooting
         */

        ArrayList<String> broadcastCommunicator = new ArrayList<String>();

        Spaceship klingons = new Spaceship("klingons");
        klingons.setBroadcastCommunicator(broadcastCommunicator);
        klingons.createCargo("Ferengi snail Juice", 200);
        klingons.createCargo("Bat'leth Klingons Sword", 200);

        Spaceship romulans = new Spaceship("romulans");
        romulans.setBroadcastCommunicator(broadcastCommunicator);
        romulans.createCargo("Borg Trash", 5);
        romulans.createCargo("Red Matter", 2);
        romulans.createCargo("Plasma Weapon", 50);

        Spaceship vulcans = new Spaceship("vulcans");
        vulcans.setBroadcastCommunicator(broadcastCommunicator);
        vulcans.createCargo("Probe", 50);
        vulcans.setPhotonTorpedoes(3);

        // Klingons shoot Romulans
        klingons.shootPhotonTorpedoes(romulans);

        // Testing
        if (romulans.getShieldProtectionInPercent() < 100) {
            // Should be 100 because the klingons do not have any photon torpedoes.
            System.out.println("<!> ERROR: testInstanceToGetShotByAPhotonTorpedo -> shootPhotonTorpedoes()" + "\n");
        }

        // Romulans shoot Klingons
        romulans.shootPhaserCannon(klingons);

        // Testing
        if (romulans.getEnergyLevelInPercent() >= 100) {
            System.out.println("<!> ERROR: testInstance -> shootPhaserCannon getEnergyLevelInPercent()" + "\n");
            System.out.println(romulans.getEnergyLevelInPercent());
        }
        if (klingons.getShieldProtectionInPercent() >= 100) {
            System.out.println("<!> ERROR: testInstanceToGetShotByAPhaserCannon -> shootPhotonTorpedoes()" + "\n");
        }

        // Vulcans send a message to everyone
        vulcans.printMessageToEveryone("Violence is not logical");

        // Klingons check their ship and their cargo
        System.out.println("klingons -> ShieldProtectionInPercent: " + klingons.getShieldProtectionInPercent() + "\n");
        klingons.showCargoList();

        klingons.printBroadcastCommunicator();
    }
}
