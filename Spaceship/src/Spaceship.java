import java.util.ArrayList;
import java.util.List;

public class Spaceship {
    private String name;
    private int photonTorpedoes;
    private int phaserCannon;
    private int androidsAmount;
    private double energyLevelInPercent;
    private double shieldProtectionInPercent;
    private double shellProtectionInPercent;
    private double lifeSupportFunctionalityInPercent;
    ArrayList<String> broadcastCommunicator = null;
    ArrayList<Cargo> cargos = new ArrayList<Cargo>();

    @Override
    public String toString() {
        return "Spaceship{" +
                "name=\"" + name + "\"" +
                ", photonTorpedoes=" + photonTorpedoes +
                ", phaserCannon=" + phaserCannon +
                ", androidsAmount=" + androidsAmount +
                ", energyLevelInPercent=" + energyLevelInPercent +
                ", shieldProtectionInPercent=" + shellProtectionInPercent +
                ", shieldProtectionInPercent=" + shieldProtectionInPercent +
                ", lifeSupportFunctionalityInPercent=" + lifeSupportFunctionalityInPercent +
                ", cargos=" + cargos +
                '}';
    }

    public Spaceship(String name) {
        this.name = name;
        this.photonTorpedoes = 0;
        this.androidsAmount = 0;
        this.energyLevelInPercent = 100;
        this.shellProtectionInPercent = 100;
        this.shieldProtectionInPercent = 100;
        this.lifeSupportFunctionalityInPercent = 100;
    }

    /*
     * Setting
     */
    public void setName(String name) {
        this.name = name;
    }

    public void setPhotonTorpedoes(int amount) {
        this.photonTorpedoes = amount;
    }

    public void setAndroidsAmount(int amount) {
        this.androidsAmount = amount;
    }

    public void setEnergyLevelInPercent(double amount) {
        this.energyLevelInPercent = amount;
    }

    public void setShieldProtectionInPercent(double amount) {
        this.shieldProtectionInPercent = amount;
    }

    public void setShellProtectionInPercent(double amount) {
        this.shieldProtectionInPercent = amount;
    }

    public void setLifeSupportFunctionalityInPercent(double amount) {
        this.lifeSupportFunctionalityInPercent = amount;
    }

    public void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
        this.broadcastCommunicator = broadcastCommunicator;
    }

    /*
     * Getting
     */

    public String getName() {
        return this.name;
    }

    public int getPhotonTorpedoes() {
        return this.photonTorpedoes;
    }

    public int getAndroidsAmount() {
        return this.androidsAmount;
    }

    public double getEnergyLevelInPercent() {
        return this.energyLevelInPercent;
    }

    public double getShieldProtectionInPercent() {
        return this.shieldProtectionInPercent;
    }

    public double getShellProtectionInPercent() {
        return this.shieldProtectionInPercent;
    }

    public double getLifeSupportFunctionalityInPercent() {
        return this.lifeSupportFunctionalityInPercent;
    }

    public ArrayList<String> getBroadcastCommunicator() {
        return this.broadcastCommunicator;
    }

    public List<Object> getAll() {
        List<Object> state = new ArrayList<Object>();
        state.add(this.name);
        state.add(this.photonTorpedoes);
        state.add(this.androidsAmount);
        state.add(this.energyLevelInPercent);
        state.add(this.shellProtectionInPercent);
        state.add(this.shieldProtectionInPercent);
        state.add(this.lifeSupportFunctionalityInPercent);
        return state;
    }

    /*
     * Cargo
     */

    public void addCargo(Cargo cargo) {
        // ToDo: only if not 0
        this.cargos.add(cargo);
    }

    public void createCargo(String kind, double amount) {
        // ToDo: only if not 0
        this.cargos.add(new Cargo(kind, amount));
    }

    public List<Object> showCargoList() {
        List<Object> cargos = new ArrayList<Object>();

        for (Cargo cargo: this.cargos) {
            List<Object> arr = new ArrayList<Object>();
            arr.add(cargo.getKind());
            arr.add(cargo.getAmount());
            cargos.add(arr);
        }
        return cargos;
    }

    /*
     * Printing
     */

    public void printAll() {
        System.out.println(": " + "Name: " + "\"" + this.getName() + "\"");
        System.out.println(": " + "PhotonTorpedoes: " + this.photonTorpedoes);
        System.out.println(": " + "AndroidsAmount: " + this.androidsAmount);
        System.out.println(": " + "EnergyLevelInPercent: " + this.energyLevelInPercent);
        System.out.println(": " + "ShellProtectionInPercent: " + this.shellProtectionInPercent);
        System.out.println(": " + "ShieldProtectionInPercent: " + this.shieldProtectionInPercent);
        System.out.println(": " + "LifeSupportFunctionalityInPercent: " + this.lifeSupportFunctionalityInPercent);
        System.out.println();
    }

    public void printCargoList() {
        for (Cargo cargo: this.cargos) {
            System.out.println(cargo.getKind() + ": " + cargo.getAmount() + "\n");
        }
    }

    public void printMessageToEveryone(String messageToEveryone) {
        this.broadcastCommunicator.add(
                "- broadcastCommunicator: " + "\"" + this.getName() + "\"" + " " + "say/says" + " " + "\"" + messageToEveryone + "\"" + "\n"
        );
        //  System.out.println(
        //      ": " + "\"" + this.getName() + "\"" + " " + "says" + " " + "\"" + messageToEveryone + "\"" + "\n"
        //  );
    }

    public void printBroadcastCommunicator() {
        this.broadcastCommunicator.forEach(System.out::println);
    }

    /*
     * Actions
     */

    public void checkShieldProtectionInPercent() {
        if (this.shieldProtectionInPercent < 0) {
            System.out.println(": " + "\"" + this.getName() + "\"" + " " + "has no shield protection. ");
            System.out.println(": " + "It does no longer exist");
            System.out.println(": " + "RIP" + " " + "\"" + this.getName() + "\"");
            }
        }

    public void successfulHit(double damage) {
        if (this.getShieldProtectionInPercent() >= damage) {
            this.setShieldProtectionInPercent(this.getShieldProtectionInPercent() - damage);
        }
        else {
            double overlap = damage - this.getShieldProtectionInPercent();
            this.setShieldProtectionInPercent(0);

            if (this.getShellProtectionInPercent() >= overlap) {
                this.setShellProtectionInPercent(this.getShellProtectionInPercent() - damage);
            }
            else {
                this.printMessageToEveryone("The systems to sustain live are destroyed");
            }
        }
        System.out.println(": " + "\"" + this.getName() + "\"" + " " + "got hit" + "\n");
        this.checkShieldProtectionInPercent();
    }

    public void shootPhotonTorpedoes(Spaceship target) {
        if (this.getPhotonTorpedoes() < 1) {
            System.out.println(": " + "\"" + this.getName() + "\"" + " " + "-=*Click*=-" + "\n");
        }

        else {
            this.photonTorpedoes =- 1;
            System.out.println(": " + "\"" + this.getName() + "\"" + " " + "shot a photon torpedo" + "\n");
            // Random Damage for 10 to 20
            target.successfulHit(Math.floor(Math.random()*(20-10+1)+10));
        }
    }

    public void shootPhaserCannon(Spaceship target) {
        if (this.getEnergyLevelInPercent() < 50) {
            System.out.println(": " + "\"" + this.getName() + "\"" + " " + "-=*Click*=-" + "\n");
            System.out.println(this.getEnergyLevelInPercent());
        }

        else {
            this.phaserCannon =- 1;
            System.out.println(": " + "\"" + this.getName() + "\"" + " " + "shot a phaser" + "\n");
            // Random Damage for 10 to 40
            target.successfulHit(Math.floor(Math.random()*(40-10+1)+10));

            this.setEnergyLevelInPercent(this.getEnergyLevelInPercent() - 50);
        }
    }
}
