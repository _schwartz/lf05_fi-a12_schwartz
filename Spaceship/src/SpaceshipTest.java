import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SpaceshipCustomTest {
    public static void main(String[] args) {
        Spaceship testInstance = new Spaceship("SpaceshipTestInstance");

        /*
         * Setting and Getting
         */

        String name = "Coca Cola";
        testInstance.setName(name);
        if (!Objects.equals(testInstance.getName(), name)) {
            System.out.println("<!> ERROR: testInstance -> name" + "\n");
        }

        int photonTorpedoes = 1;
        testInstance.setPhotonTorpedoes(photonTorpedoes);
        if (testInstance.getPhotonTorpedoes() != photonTorpedoes) {
            System.out.println("<!> ERROR: testInstance -> photonTorpedoes" + "\n");
        }

        int androidsAmount = 1;
        testInstance.setAndroidsAmount(androidsAmount);
        if (testInstance.getAndroidsAmount() != androidsAmount) {
            System.out.println("<!> ERROR: testInstance -> androidsAmount" + "\n");
        }

        double energyLevelInPercent = 90;
        testInstance.setEnergyLevelInPercent(energyLevelInPercent);
        if (testInstance.getEnergyLevelInPercent() != energyLevelInPercent) {
            System.out.println("<!> ERROR: testInstance -> energyLevelInPercent" + "\n");
        }

        double shieldProtectionInPercent = 91;
        testInstance.setShieldProtectionInPercent(shieldProtectionInPercent);
        if (testInstance.getShieldProtectionInPercent() != shieldProtectionInPercent) {
            System.out.println("<!> ERROR: testInstance -> shieldProtectionInPercent" + "\n");
        }

        double lifeSupportFunctionalityInPercent = 92;
        testInstance.setLifeSupportFunctionalityInPercent(lifeSupportFunctionalityInPercent);
        if (testInstance.getLifeSupportFunctionalityInPercent() != lifeSupportFunctionalityInPercent) {
            System.out.println("<!> ERROR: testInstance -> lifeSupportFunctionalityInPercent" + "\n");
        }

        testInstance.getAll();
        List<Object> testState = new ArrayList<Object>();
        testState.add(name);
        testState.add(photonTorpedoes);
        testState.add(androidsAmount);
        testState.add(energyLevelInPercent);
        testState.add(shieldProtectionInPercent);
        testState.add(lifeSupportFunctionalityInPercent);

        if (testState.equals(testInstance.getLifeSupportFunctionalityInPercent())) {
            System.out.println("<!> ERROR: testInstance -> LifeSupportFunctionalityInPercent" + "\n");
        }

        /*
         * Cargo
         */

        String cargoTestKind = "AirIsCargoToo";
        double cargoTestAmount = 42;

        List<Object> testCargos = new ArrayList<Object>();
        List<Object> testArr = new ArrayList<Object>();
        testArr.add(cargoTestKind);
        testArr.add(cargoTestAmount);
        testCargos.add(testArr);

        testInstance.createCargo(cargoTestKind, cargoTestAmount);

        System.out.println(testCargos);
        System.out.println(testInstance.showCargoList());

        /* I do not know why but the results aren't the same */
        if (testInstance.showCargoList() != testCargos) {
            System.out.println("<!> ERROR: testInstance -> cargos" + "\n");
        }

        /*
         * Printing
         */

        testInstance.printAll();

        testInstance.printCargoList();
    }
}
